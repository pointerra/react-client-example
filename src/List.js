import React, {useEffect, useState} from 'react';
import jwt from "@pointerra/pointerra-jwt";

const List = ({onCloudSelected}) => {
  const [cloudList, setCloudList] = useState([]);
  const [cloudInfo, setCloudInfo] = useState({});

  function pagerButton(url, text) {
    if (url) {
      return <button className="Pager-button" type="button" onClick={() => loadClouds(url)}>{text}</button>
    } else {
      return <button className="Pager-button" disabled type="button">{text}</button>
    }
  }

  async function loadClouds(url) {
    try {
      const response = await fetch(url, {
        headers: await jwt.headers()
      })

      const {
        data,
        total,
        current_page,
        next_page_url,
        previous_page_url,
        last_page,
      } = await response.json()

      setCloudInfo({
        total,
        current_page,
        next_page_url,
        previous_page_url,
        last_page,
      })

      setCloudList(data)
    } catch (err) {
      console.error(err)
      setCloudList([])
    }
  }

  useEffect(() => {
    loadClouds('https://app.pointerra.io/api/pointclouds/')
  }, [])


  return (
    <div className="List">
      <h1 className="List-header">{cloudInfo.total} results</h1>
      <ul className="List-clouds">
        {cloudList && cloudList.map(cloud => <li key={cloud.pointcloud_id}>
          <button type="button" className="List-entry"
                  onClick={() => onCloudSelected(cloud.pointcloud_id)}>{cloud.name}</button>
        </li>)}
      </ul>
      {cloudInfo.total > 10 &&
        <div className="Pager">
          {pagerButton(cloudInfo.previous_page_url, "❮ Prev")}
          {cloudInfo.current_page} / {cloudInfo.last_page}
          {pagerButton(cloudInfo.next_page_url, "Next ❯")}
        </div>
      }
    </div>
  )
}

export default List;

import React, {useEffect, useState} from 'react';

import loadClient, {Plugins} from '@pointerra/pointerra-bootstrap';
import jwt from "@pointerra/pointerra-jwt";

const Viewer = ({cloudId}) => {
  const [api, setApi] = useState(null);

  useEffect(() => {
    loadClient({
      targetElement: "#cloud-viewer",
      jwt,
      plugins: [
        Plugins.LeafletMap(),
        // Plugins.Datasets(),
        // Plugins.PanoViewer(),
        // Plugins.PhotosViewer(),
        // Plugins.SphericalImages(),
      ],
      viewer: {
        enableTools: false,
        showFullscreenButton: true,
        showLeftPanel: false,
        showOptionsButton: true,
      },
    }).then(setApi)
  }, [])

  useEffect(() => {
    if (api && cloudId) {
      api.pointcloud.load(cloudId)
    }
  }, [cloudId, api])

  return (
    <div id="cloud-viewer" className="Viewer" tabIndex="0"/>
  );
}

export default Viewer;

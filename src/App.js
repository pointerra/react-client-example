import React, {useState} from 'react';
import './App.css';
import Viewer from "./Viewer";
import List from "./List";

function App() {
  const [selectedCloudId, setSelectedCloudId] = useState(null);

  return (
    <main className="App">
      <List onCloudSelected={setSelectedCloudId}/>

      <Viewer cloudId={selectedCloudId}/>
    </main >
  );
}

export default App;
